# Karomi_Lectures
This repository contains all the required lecture notes that is taken as part of Karomi's Visual Computing course along with python codes. All copyrights belong to Karomi Technology Private Limited. If you have any queries contact sreenivas@karomi.com


## Required python packages to run code in this repository
* ```numpy```
* ```opencv-python (cv2)```
* ```argsparse (default python library)``` 


## Python coding convention used
Style: PEP 8

Docstring type: Google docstring
