"""
.. module:: sample module
   :platform: Windows
   :synopsis: This module does simple details

.. module author:: Sreenivas Narasimha Murali (sreenivas@karomi.com)
.. copyrights: Karomi Technology Private Limited
.. date created: 02/08/2018

"""
import cv2
import numpy as np


def pass_image(inp):
    """
    this function passes an image
    :param inp: Image of 3-D array
    :return: returns the same image
    """
    output = inp
    return output


def array_index_modification(inp_array):
    """
    Modified input array and returns new array
    :param inp_array: input array
    :return: modified output array
    """
    modified_array = inp_array
    modified_array[1] = 0
    return modified_array


if __name__ == "__main__":
    im = cv2.imread('lenna.png')
    out = pass_image(im)
    cv2.imshow('image', out)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    input_array = np.array([10, 20, 40])
    output_array = array_index_modification(input_array)
    print(output_array)
